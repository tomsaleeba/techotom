package com.wordpress.techotom.graceful;

import java.io.Closeable;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RunMe implements Closeable{
    
	private static final Logger logger = Logger.getLogger(RunMe.class);
	private static final String APPLICATION_CONTEXT_XML_LOCATION = "com/wordpress/techotom/graceful/application-context.xml";
	private static final int SECONDS_TO_RUN_FOR = 5;
	private static final int ONE_SECOND_MS = 1000;

	public static void main(String[] args) throws InterruptedException {
		logger.info("Launching the graceful-shutdown application");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(APPLICATION_CONTEXT_XML_LOCATION);
		RunMe runMe = context.getBean(RunMe.class);
		runMe.run();
		context.close();
    }

	public void run() throws InterruptedException {
		logger.info("Program is now running.");
		logger.info(String.format("I'll print a heartbeat for %d seconds then shutdown, unless you kill me first.", SECONDS_TO_RUN_FOR));
		logger.info("The close() method should ALWAYS be called. You'll know because it prints a logger message.");
		for (int i = 0;i < SECONDS_TO_RUN_FOR;i++) {
			logger.info("Still alive and kicking");
			Thread.sleep(ONE_SECOND_MS);
		}
		logger.info("OK, time to shutdown. I'll use System.exit() for this.");
		System.exit(0);
	}

	@Override
	public void close() throws IOException {
		logger.info("close() was called, yay!");
	}
}

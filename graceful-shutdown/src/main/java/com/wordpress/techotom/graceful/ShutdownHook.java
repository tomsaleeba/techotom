package com.wordpress.techotom.graceful;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;

public class ShutdownHook implements ApplicationContextAware {

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		((ConfigurableApplicationContext) applicationContext).registerShutdownHook();
	}
}

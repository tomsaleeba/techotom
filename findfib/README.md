A quick program to find Fibonacci numbers.

Triggered by the challenge from CGPGrey https://youtu.be/kIMSFZH3Xe4?t=56s. Not that I've got the skills to apply for that, I just wanted to make a program.

If you're on Linux or OSX, run the program with

        ./run
...otherwise you can do it manually (still not that hard):

        javac FindFib.java
        java FindFib

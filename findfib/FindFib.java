import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class FindFib {

	public static void main(String[] args) {
		int limit = 101;
		List<BigInteger> result = new LinkedList<>();
		result.add(new BigInteger("0"));
		result.add(new BigInteger("1"));
		for (int i = result.size()-1;i < limit; i++) {
			BigInteger previousTerm = result.get(i);
			BigInteger termBeforeThat = result.get(i-1);
			BigInteger next = previousTerm.add(termBeforeThat);
			System.out.println("The Fibonacci number at " + (i+1) + " is " + next + " (or " + previousTerm + " + " + termBeforeThat + ")");
			result.add(next);
		}
	}
}

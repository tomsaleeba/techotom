package org.wordpress.techotom;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Example showing how a file backed model reads what is in the file.
 */
public class ExampleJena_FileBasedModel02 {

	private static final String MODEL_NAME = "model1.ttl";

	public static void main(String[] args) throws Throwable {
		Path tempDirFile = Files.createTempDirectory("ExampleJena_WriteToFile");
		File modelFile = Paths.get(tempDirFile.toString(), MODEL_NAME).toFile();
		System.out.println("Using model file: " + modelFile.getAbsolutePath());
		FileWriter w = new FileWriter(modelFile);
		w.write("@prefix :      <http://techotom.wordpress.org#> .\n" +
				":Earth  :has_star  \"Sol\" .\n");
		w.close();
		Model model = ModelFactory.createFileModelMaker(tempDirFile.toString()).openModel(MODEL_NAME);
		Resource s = model.createResource("http://techotom.wordpress.org#Earth");
		String o = s.getProperty(model.createProperty("http://techotom.wordpress.org#has_star")).getString();
		System.out.println("Value of statement is " + o);
		model.close();
	}
}

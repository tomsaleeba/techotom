package org.wordpress.techotom;

import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * Example showing how we can infer all subclasses of a super type
 */
public class ExampleJena_Rdfs_SubClassOf01 {

	public static void main(String[] args) throws Throwable {
		Model model = ModelFactory.createDefaultModel();
		String base = "http://techotom.wordpress.org#";
		model.setNsPrefix("", base);
		Resource habitablePlanetType = model.createResource(base + "HabitablePlanet");
		Resource s = model.createResource(base + "Earth", habitablePlanetType);
		Property p = model.createProperty(base + "has_star");
		model.add(s, p, "Sol");
		Resource planetType = model.createResource(base + "Planet");
		Model schema = ModelFactory.createOntologyModel();
		schema.add(habitablePlanetType, RDFS.subClassOf, planetType);
		InfModel inf = ModelFactory.createRDFSModel(schema, model);
		StmtIterator result = inf.listStatements(new SimpleSelector(null, RDF.type, planetType)); // finds our HabitablePlanet
		for (;result.hasNext();) {
			Statement curr = result.next();
			System.out.println(curr);
		}
	}
}

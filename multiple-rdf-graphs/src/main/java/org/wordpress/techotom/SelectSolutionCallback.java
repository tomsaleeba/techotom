package org.wordpress.techotom;

import com.hp.hpl.jena.query.QuerySolution;

public interface SelectSolutionCallback {
	void handleSolutionBinding(QuerySolution soln);
}
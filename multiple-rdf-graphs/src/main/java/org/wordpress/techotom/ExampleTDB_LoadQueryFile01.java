package org.wordpress.techotom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDBFactory;

/**
 * Example showing how to read a SPARQL query from a file on the classpath. 
 */
public class ExampleTDB_LoadQueryFile01 {

	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_LoadQueryFile01.class);
	private Dataset dataset;
	
	public static void main(String[] args) {
		new ExampleTDB_LoadQueryFile01().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		Dataset ds = TDBFactory.createDataset();
		Model model = ds.getDefaultModel();
		Resource s = model.createResource("#someSubject");
		Property p = model.createProperty("#has_prop");
		model.add(s, p, "a string literal");
		dataset = ds;
	}
	
	private void execQuery() {
		Query query = QueryFactory.read("org/wordpress/techotom/selectAll.rq"); // Reading from a file on the classpath
		try (QueryExecution qexec = QueryExecutionFactory.create(query, dataset)) {
			ResultSet results = qexec.execSelect();
			if (!results.hasNext()) {
				throw new RuntimeException("No results were returned in the solution for the query: " + query.toString());
			}
			for (; results.hasNext();) {
				QuerySolution soln = results.next();
				String msg = String.format("%s %s %s", soln.get("s"), soln.get("p"), soln.get("o"));
				logger.info(msg);
			}
		}
	}
}

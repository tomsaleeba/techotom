package org.wordpress.techotom;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Example showing how to create a model with a statement and write it to the console.
 */
public class ExampleJena_WriteToConsole {

	public static void main(String[] args) {
		Model model = ModelFactory.createDefaultModel();
		String base = "http://techotom.wordpress.org#";
		model.setNsPrefix("", base);
		Resource s = model.createResource(base + "Earth");
		Property p = model.createProperty(base + "has_star");
		model.add(s, p, "Sol");
		model.write(System.out, "TTL");
	}
}

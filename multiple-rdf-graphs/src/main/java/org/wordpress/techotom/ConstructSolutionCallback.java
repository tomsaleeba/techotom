package org.wordpress.techotom;

import com.hp.hpl.jena.rdf.model.Model;

public interface ConstructSolutionCallback {
	void handleResult(Model result);
}
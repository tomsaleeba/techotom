package org.wordpress.techotom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.FileManager;

/**
 * TDB in-memory instance
 * Two named graphs
 * Loaded from files
 * UnionDefaultGraph symbol NOT set
 * SELECT query using FROM NAMED for both graphs
 * Query run against Dataset
 */
public class ExampleTDB_SelectAll01 {

	private static final String MODEL2_NAME = "http://techotom.wordpress.org/model2";
	private static final String MODEL1_NAME = "http://techotom.wordpress.org/model1";
	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_SelectAll01.class);
	private Dataset dataset;
	
	public static void main(String[] args) {
		new ExampleTDB_SelectAll01().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		FileManager fm = FileManager.get();
        fm.addLocatorClassLoader(getClass().getClassLoader());
        Model model1 = fm.loadModel("org/wordpress/techotom/model1.ttl");
        Model model2 = fm.loadModel("org/wordpress/techotom/model2.ttl");
		Dataset ds = TDBFactory.createDataset();
		ds.addNamedModel(MODEL1_NAME, model1);
		ds.addNamedModel(MODEL2_NAME, model2);
		dataset = ds;
	}
	
	private void execQuery() {
		String query =
				"SELECT * " +
				"FROM NAMED <" + MODEL1_NAME + "> " +
				"FROM NAMED <" + MODEL2_NAME + "> " +
				"WHERE { " +
				"  GRAPH ?g " +
				"    {?s ?p ?o .} " +
				"}";
		QueryRunner.execSelect(query, dataset, new SelectSolutionCallback() {
			@Override
			public void handleSolutionBinding(QuerySolution soln) {
				String msg = String.format("%s %s %s %s", soln.get("g"), soln.get("s"), soln.get("p"), soln.get("o"));
				logger.info(msg);
			}
		});
	}
}

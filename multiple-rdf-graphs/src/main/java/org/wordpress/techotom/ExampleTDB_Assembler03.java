package org.wordpress.techotom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;

/**
 * TDB in-memory instance
 * Two external files loaded into the default graph
 * Loaded using assembler
 * UnionDefaultGraph symbol NOT set
 * SELECT query without FROM or FROM NAMED
 * Query run against default graph
 */
public class ExampleTDB_Assembler03 {

	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_Assembler03.class);
	private Dataset dataset;
	
	public static void main(String[] args) {
		new ExampleTDB_Assembler03().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		Dataset ds = DatasetFactory.assemble("org/wordpress/techotom/assembler2.ttl");
		dataset = ds;
	}
	
	private void execQuery() {
		String query =
				"SELECT * " +
				"WHERE { " +
				"   ?s ?p ?o . " +
				"}";
		Model defaultGraph = dataset.getDefaultModel();
		QueryRunner.execSelect(query, defaultGraph, new SelectSolutionCallback() {
			@Override
			public void handleSolutionBinding(QuerySolution soln) {
				String msg = String.format("%s %s %s", soln.get("s"), soln.get("p"), soln.get("o"));
				logger.info(msg);
			}
		});
	}
}

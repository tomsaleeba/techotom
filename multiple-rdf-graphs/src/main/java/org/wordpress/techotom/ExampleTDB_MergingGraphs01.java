package org.wordpress.techotom;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;

/**
 * Example showing how two entities with the same name but in
 * different graphs are merged. 
 */
public class ExampleTDB_MergingGraphs01 {

	private Dataset dataset;
	
	public static void main(String[] args) {
		new ExampleTDB_MergingGraphs01().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		TDB.getContext().set(TDB.symUnionDefaultGraph, true);
		Dataset ds = TDBFactory.createDataset();
		Model model1 = ModelFactory.createMemModelMaker().createFreshModel();
		Model model2 = ModelFactory.createMemModelMaker().createFreshModel();
		Resource s = model1.createResource("#someSubject"); // Note: doesn't matter which model we use to create resources
		Property pBoth = model1.createProperty("#has_prop_in_both_graphs");
		Property pOne = model1.createProperty("#has_prop_in_model1");
		Property pTwo = model1.createProperty("#has_prop_in_model2");
		model1.add(s, pBoth, "pBoth-model1");
		model2.add(s, pBoth, "pBoth-model2");
		model1.add(s, pOne, "pOne-Model1");
		model2.add(s, pTwo, "pTwo-Model2");
		ds.addNamedModel("#model2", model2); // Note: only add the models when they're fully populated.
		ds.addNamedModel("#model1", model1); //       Adding the model to the graph then adding more triples means they won't be in Dataset.
		dataset = ds;
	}
	
	private void execQuery() {
		Query query = QueryFactory.create(
				"DESCRIBE ?s " +
				"WHERE { ?s ?p ?o . }");
		try (QueryExecution qexec = QueryExecutionFactory.create(query, dataset)) {
			Model result = qexec.execDescribe();
			result.write(System.out, "TURTLE");
		}
	}
}

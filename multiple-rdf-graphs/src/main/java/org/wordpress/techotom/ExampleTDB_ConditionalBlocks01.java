package org.wordpress.techotom;

import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;

/**
 * Example where you want to do { ...pattern... } OR { ...pattern... }.
 * That is, doing an OR on blocks, not just properties. In this instance we
 * want to get the prefferedProp if it exists, otherwise get the backupProp.
 */
public class ExampleTDB_ConditionalBlocks01 {

	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_ConditionalBlocks01.class);
	private Model model;
	
	public static void main(String[] args) {
		new ExampleTDB_ConditionalBlocks01().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		String triples = 
				"PREFIX : <http://techotom.wordpress.org#> " +
				":a :preferredProp 'a-pref' . " +
				":b :preferredProp 'b-pref' . " +
				":b :backupProp 'b-back' . " +
				":c :backupProp 'c-back' . ";
		InputStream stream = IOUtils.toInputStream(triples, Charset.defaultCharset());
		Model result = ModelFactory.createDefaultModel();
		result.read(stream, null, "TTL");
		model = result;
		logger.info("Loaded model:");
		for (Statement curr : model.listStatements().toList()) {
			logger.info(curr.toString());
		}
		logger.info("");
	}
	
	private void execQuery() {
		String query =
				"PREFIX : <http://techotom.wordpress.org#> " +
				"CONSTRUCT { " +
				"  ?s :value ?pref ." +
				"  ?s :value ?back ." +
				"}" +
				"WHERE { " +
				"  ?s !:blah ?o . " +
				"  OPTIONAL {?s :preferredProp ?pref .} " +
				"  OPTIONAL {?s :backupProp ?backupTemp .} " +
				"  BIND(IF(BOUND(?pref), ?nothing, ?backupTemp) as ?back) . " +
				"}";
		QueryRunner.execConstruct(query, model, new ConstructSolutionCallback() {
			@Override
			public void handleResult(Model result) {
				logger.info("Query result:");
				for (Statement curr : result.listStatements().toList()) {
					logger.info(curr.toString());
				}
			}
		});
	}
}

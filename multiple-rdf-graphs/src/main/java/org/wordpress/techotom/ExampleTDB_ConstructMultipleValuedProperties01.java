package org.wordpress.techotom;

import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * Example where you want to do a CONSTRUCT query and make multi-valued
 * properties in the result. In this case, we want to combined the :firstName
 * and :middleName values into a multi-valued :givenNames property value.
 */
public class ExampleTDB_ConstructMultipleValuedProperties01 {

	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_ConstructMultipleValuedProperties01.class);
	private Model model;
	
	public static void main(String[] args) {
		new ExampleTDB_ConstructMultipleValuedProperties01().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		String triples = 
				"PREFIX : <http://techotom.wordpress.org#> " +
				":turing :firstName 'Alan' . " +
				":turing :middleName 'Mathison' . " +
				":turing :surName 'Turing' . " +
				":tyson :firstName 'Neil' . " +
				":tyson :middleName 'deGrasse' . " +
				":tyson :surName 'Tyson' . ";
		InputStream stream = IOUtils.toInputStream(triples, Charset.defaultCharset());
		Model result = ModelFactory.createDefaultModel();
		result.read(stream, null, "TTL");
		model = result;
		logger.info("Loaded model:");
		model.write(System.out, "TTL");
	}
	
	private void execQuery() {
		String query =
				"PREFIX : <http://techotom.wordpress.org#> " +
				"CONSTRUCT { " +
				"  ?subject :givenNames ?f ." +
				"  ?subject :givenNames ?m ." +
				"  ?subject :surName ?s ." +
				"}" +
				"WHERE { " +
				"  ?subject :firstName ?f . " +
				"  ?subject :middleName ?m . " +
				"  ?subject :surName ?s . " +
				"}";
		QueryRunner.execConstruct(query, model, new ConstructSolutionCallback() {
			@Override
			public void handleResult(Model result) {
				logger.info("Query result:");
				result.write(System.out, "TTL");
			}
		});
	}
}

package org.wordpress.techotom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.query.QuerySolution;

/**
 * TDB in-memory instance
 * Two named graphs
 * Loaded using assembler
 * UnionDefaultGraph symbol NOT set
 * SELECT query using FROM NAMED for both graphs
 * Query run against Dataset
 */
public class ExampleTDB_Assembler01 {

	private static final String MODEL1_NAME = "http://techotom.wordpress.org/model1"; // These need to match the names in the assembler file
	private static final String MODEL2_NAME = "http://techotom.wordpress.org/model2";
	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_Assembler01.class);
	private Dataset dataset;
	
	public static void main(String[] args) {
		new ExampleTDB_Assembler01().run();
	}

	private void run() {
		populateRepo();
		execQuery();
	}

	private void populateRepo() {
		Dataset ds = DatasetFactory.assemble("org/wordpress/techotom/assembler1.ttl");
		dataset = ds;
	}
	
	private void execQuery() {
		String query =
				"SELECT * " +
				"FROM NAMED <" + MODEL1_NAME + "> " +
				"FROM NAMED <" + MODEL2_NAME + "> " +
				"WHERE { " +
				"  GRAPH ?g " +
				"    {?s ?p ?o .} " +
				"}";
		QueryRunner.execSelect(query, dataset, new SelectSolutionCallback() {
			@Override
			public void handleSolutionBinding(QuerySolution soln) {
				String msg = String.format("%s %s %s %s", soln.get("g"), soln.get("s"), soln.get("p"), soln.get("o"));
				logger.info(msg);
			}
		});
	}
}

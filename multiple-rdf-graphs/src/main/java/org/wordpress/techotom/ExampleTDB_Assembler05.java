package org.wordpress.techotom;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDBFactory;

/**
 * Example that creates a TDB instance, populates it then closes the connection.
 * Then we open the TDB with an assembler file but don't set the unionDefaultGraph
 * so we instead retrieve the unionGraph by name and query it.
 */
public class ExampleTDB_Assembler05 {

	private static final String TDB_DIR_PATH = "target/ExampleTDB_Assembler05";
	private static final Logger logger = LoggerFactory.getLogger(ExampleTDB_Assembler05.class);
	private Dataset dataset;
	
	public static void main(String[] args) throws IOException {
		new ExampleTDB_Assembler05().run();
	}

	private void run() throws IOException {
		createTdbRepoOnDisk();
		loadTdbRepoWithAssembler();
		execQuery();
	}

	private void createTdbRepoOnDisk() throws IOException {
		FileUtils.deleteDirectory(new File(TDB_DIR_PATH));
		Dataset ds = TDBFactory.createDataset(TDB_DIR_PATH);
		Model model1 = ModelFactory.createMemModelMaker().createFreshModel();
		Model model2 = ModelFactory.createMemModelMaker().createFreshModel();
		Resource s = model1.createResource("#someSubject"); // Note: doesn't matter which model we use to create resources
		Property pBoth = model1.createProperty("#has_prop_in_both_graphs");
		Property pOne = model1.createProperty("#has_prop_in_model1");
		Property pTwo = model1.createProperty("#has_prop_in_model2");
		model1.add(s, pBoth, "pBoth-model1");
		model2.add(s, pBoth, "pBoth-model2");
		model1.add(s, pOne, "pOne-Model1");
		model2.add(s, pTwo, "pTwo-Model2");
		ds.addNamedModel("#model2", model2); // Note: only add the models when they're fully populated.
		ds.addNamedModel("#model1", model1);
		ds.close();
	}

	private void loadTdbRepoWithAssembler() {
		Dataset ds = TDBFactory.assembleDataset("org/wordpress/techotom/assembler5.ttl");
		dataset = ds;
	}
	
	private void execQuery() {
		String query =
				"SELECT * " +
				"WHERE { " +
				"   ?s ?p ?o . " +
				"}";
		Model defaultGraph = dataset.getNamedModel("urn:x-arq:UnionGraph");
		QueryRunner.execSelect(query, defaultGraph, new SelectSolutionCallback() {
			@Override
			public void handleSolutionBinding(QuerySolution soln) {
				String msg = String.format("%s %s %s", soln.get("s"), soln.get("p"), soln.get("o"));
				logger.info(msg);
			}
		});
	}
}

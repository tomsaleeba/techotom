package org.wordpress.techotom;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;

public class QueryRunner {
	public static void execSelect(String queryString, Dataset ds, SelectSolutionCallback callback) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, ds)) {
			ResultSet results = qexec.execSelect();
			if (!results.hasNext()) {
				throw new RuntimeException("No results were returned in the solution for the query: " + query.toString());
			}
			for (; results.hasNext();) {
				QuerySolution soln = results.next();
				callback.handleSolutionBinding(soln);
			}
		}
	}
	
	public static void execSelect(String queryString, Model model, SelectSolutionCallback callback) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			ResultSet results = qexec.execSelect();
			if (!results.hasNext()) {
				throw new RuntimeException("No results were returned in the solution for the query: " + query.toString());
			}
			for (; results.hasNext();) {
				QuerySolution soln = results.next();
				callback.handleSolutionBinding(soln);
			}
		}
	}
	
	public static void execConstruct(String queryString, Model model, ConstructSolutionCallback callback) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			Model result = qexec.execConstruct();
			if (result.isEmpty()) {
				throw new RuntimeException("No results were returned in the solution for the query: " + query.toString());
			}
			callback.handleResult(result);
		}
	}
}
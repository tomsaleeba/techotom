package org.wordpress.techotom;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Example showing how to create a model with a statement and write it to a file.
 */
public class ExampleJena_WriteToFile {

	public static void main(String[] args) throws Throwable {
		Model model = ModelFactory.createDefaultModel();
		String base = "http://techotom.wordpress.org#";
		model.setNsPrefix("", base);
		Resource s = model.createResource(base + "Earth");
		Property p = model.createProperty(base + "has_star");
		model.add(s, p, "Sol");
		File tempFile = Files.createTempFile("ExampleJena_WriteToFile", "ttl").toFile();
		FileWriter w = new FileWriter(tempFile);
		System.out.println("Writing model to " + tempFile.getAbsolutePath());
		model.write(w, "TTL");
		w.close();
	}
}

package org.wordpress.techotom;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Example showing how to create a model that is backed by a file.
 */
public class ExampleJena_FileBasedModel01 {

	private static final String MODEL_NAME = "model1.ttl";

	public static void main(String[] args) throws Throwable {
		String tempDir = Files.createTempDirectory("ExampleJena_WriteToFile").toString();
		Model model = ModelFactory.createFileModelMaker(tempDir).createModel(MODEL_NAME);
		String base = "http://techotom.wordpress.org#";
		model.setNsPrefix("", base);
		Resource s = model.createResource(base + "Earth");
		Property p = model.createProperty(base + "has_star");
		model.add(s, p, "Sol");
		model.close();
		System.out.println("Model will be on disk at " + Paths.get(tempDir, MODEL_NAME).toAbsolutePath());
	}
}

package com.wordpress.techotom.vac;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RunMe {
    
	private static final Logger logger = Logger.getLogger(RunMe.class);
	private static final String APPLICATION_CONTEXT_XML_LOCATION = "com/wordpress/techotom/vac/application-context.xml";

	private String wiredUsingXmlSpel;
	private String wiredUsingXmlPlaceholder;

	@Value("${techotom.some.property.spel}")
	private String wiredUsingPlaceholder;
	
	@Value("#{systemProperties['techotom.some.property.spel']}")
	private String wiredUsingSpel;
	
	public static void main(String[] args) {
		String spelProperty = System.getProperty("techotom.some.property.spel");
		String xmlProperty = System.getProperty("techotom.some.property.xml");
		logger.info("We're outside the application context but we could use the System.getProperty() way:");
		logger.info("System.getProperty(): techotom.some.property.spel=" + spelProperty);
		logger.info("System.getProperty(): techotom.some.property.xml=" + xmlProperty);
		logger.info("Loading bean context");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(APPLICATION_CONTEXT_XML_LOCATION);
		RunMe runMe = context.getBean(RunMe.class);
		runMe.printProperties();
		context.close();
    }

	public void printProperties() {
		logger.info("Now we're running inside the application context so we'll rely on Spring to wire the properties in.");
		logger.info("@Value Spel version:        techotom.some.property.spel=" + wiredUsingSpel);
		logger.info("@Value Placeholder version: techotom.some.property.spel=" + wiredUsingPlaceholder);
		logger.info("XML Spel version:           techotom.some.property.xml=" + wiredUsingXmlSpel);
		logger.info("XML Placeholder version:    techotom.some.property.xml=" + wiredUsingXmlPlaceholder);
	}

	public void setWiredUsingXmlSpel(String wiredUsingXmlSpel) {
		this.wiredUsingXmlSpel = wiredUsingXmlSpel;
	}

	public void setWiredUsingXmlPlaceholder(String wiredUsingXmlPlaceholder) {
		this.wiredUsingXmlPlaceholder = wiredUsingXmlPlaceholder;
	}
}

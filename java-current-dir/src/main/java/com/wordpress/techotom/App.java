package com.wordpress.techotom;

import java.nio.file.Path;
import java.nio.file.Paths;

public class App {
    
	public static void main( String[] args ) {
        Path currDir = Paths.get("");
		System.out.println("Current directory: " + currDir.toAbsolutePath().toString());
    }
}
